const data = [
	{
		id: '106',
		name: 'Black Panther',
		powerstats: {
			intelligence: '88',
			strength: '16',
			speed: '30',
			durability: '60',
			power: '41',
			combat: '100',
		},
		biography: {
			'full-name': "T'Challa",
			'alter-egos': 'No alter egos found.',
			aliases: [
				'Mr. Okonkwo',
				'The Man Without Fear',
				'Luke Charles',
				'Black Leopard',
				'the Client',
				'Coal Tiger',
				'has impersonated Daredevil and others on occasion',
			],
			'place-of-birth': 'Wakanda, Africa',
			'first-appearance': 'Fantastic Four Vol. 1 #52 (1966)',
			publisher: 'Marvel Comics',
			alignment: 'good',
		},
		appearance: {
			gender: 'Male',
			race: 'Human',
			height: ["6'0", '183 cm'],
			weight: ['200 lb', '90 kg'],
			'eye-color': 'Brown',
			'hair-color': 'Black',
		},
		work: {
			occupation:
				'King and Chieftain of Wakanda, scientist; former school teacher',
			base: 'Wakanda, Mobile',
		},
		connections: {
			'group-affiliation':
				"Formerly Fantastic Four, Secret Avengers, Avengers, Pendragons, Queen's Vengeance, former Fantastic Force financier",
			relatives:
				"Bashenga (paternal ancestor, deceased), Azzuri the Wise (paternal grandfather, deceased), Nanali (paternal grandmother, deceased), Chanda (paternal grandfather, presumably deceased), T�Chaka (father, deceased), S'Yan (uncle, deceased), N�Yami (mother, deceased), Ramonda (stepmother), Hunter (adopted brother), Jakarra (half-brother), Shuri (sister), Ororo Munroe (wife), Joshua Itobo, Ishanta, Zuni, M'Koni, T'Shan (cousins), Wheeler (cousin by marriage, deceased), Billy Wheeler (1st cousin once removed)",
		},
		image: {
			url: 'https://www.superherodb.com/pictures2/portraits/10/100/247.jpg',
		},
	},
	{
		id: '644',
		name: 'Superman',
		powerstats: {
			intelligence: '94',
			strength: '100',
			speed: '100',
			durability: '100',
			power: '100',
			combat: '85',
		},
		biography: {
			'full-name': 'Clark Kent',
			'alter-egos': 'Superman Prime One-Million',
			aliases: [
				'Clark Joseph Kent',
				'The Man of Steel',
				'the Man of Tomorrow',
				'the Last Son of Krypton',
				'Big Blue',
				'the Metropolis Marvel',
				'the Action Ace',
			],
			'place-of-birth': 'Krypton',
			'first-appearance': 'ACTION COMICS #1',
			publisher: 'Superman Prime One-Million',
			alignment: 'good',
		},
		appearance: {
			gender: 'Male',
			race: 'Kryptonian',
			height: ["6'3", '191 cm'],
			weight: ['225 lb', '101 kg'],
			'eye-color': 'Blue',
			'hair-color': 'Black',
		},
		work: {
			occupation: 'Reporter for the Daily Planet and novelist',
			base: 'Metropolis',
		},
		connections: {
			'group-affiliation':
				'Justice League of America, The Legion of Super-Heroes (pre-Crisis as Superboy); Justice Society of America (pre-Crisis Earth-2 version); All-Star Squadron (pre-Crisis Earth-2 version)',
			relatives:
				'Lois Lane (wife), Jor-El (father, deceased), Lara (mother, deceased), Jonathan Kent (adoptive father), Martha Kent (adoptive mother), Seyg-El (paternal grandfather, deceased), Zor-El (uncle, deceased), Alura (aunt, deceased), Supergirl (Kara Zor-El, cousin), Superboy (Kon-El/Conner Kent, partial clone)',
		},
		image: {
			url: 'https://www.superherodb.com/pictures2/portraits/10/100/791.jpg',
		},
	},
	{
		id: '620',
		name: 'Spider-Man',
		powerstats: {
			intelligence: '90',
			strength: '55',
			speed: '67',
			durability: '75',
			power: '74',
			combat: '85',
		},
		biography: {
			'full-name': 'Peter Parker',
			'alter-egos': 'No alter egos found.',
			aliases: [
				'Spiderman',
				'Bag-Man',
				'Black Marvel',
				'Captain Universe',
				'Dusk',
				'Green Hood',
				'Hornet',
				'Mad Dog 336',
				'Peter Palmer',
				'Prodigy',
				'Ricochet',
				'Scarlet Spider',
				'Spider-Boy',
				'Spider-Hulk',
				'Spider-Morphosis',
			],
			'place-of-birth': 'New York, New York',
			'first-appearance': 'Amazing Fantasy #15',
			publisher: 'Marvel Comics',
			alignment: 'good',
		},
		appearance: {
			gender: 'Male',
			race: 'Human',
			height: ["5'10", '178 cm'],
			weight: ['165 lb', '74 kg'],
			'eye-color': 'Hazel',
			'hair-color': 'Brown',
		},
		work: {
			occupation: 'Freelance photographer, teacher',
			base: 'New York, New York',
		},
		connections: {
			'group-affiliation':
				'Member of the Avengers, formerly member of Outlaws, alternate Fantastic Four',
			relatives:
				'Richard Parker (father, deceased), Mary Parker(mother, deceased), Benjamin Parker (uncle, deceased), May Parker (aunt), Mary Jane Watson-Parker (wife), May Parker (daughter, allegedly deceased)',
		},
		image: {
			url: 'https://www.superherodb.com/pictures2/portraits/10/100/133.jpg',
		},
	},
	{
		id: '157',
		name: 'Captain Marvel',
		powerstats: {
			intelligence: '84',
			strength: '88',
			speed: '71',
			durability: '95',
			power: '100',
			combat: '90',
		},
		biography: {
			'full-name': 'Carol Danvers',
			'alter-egos': 'Binary, Warbird',
			aliases: [
				'Ace',
				'Binary',
				'Lady Marvel',
				'Warbird',
				'others used during her espionage career',
			],
			'place-of-birth': 'Boston, Massachusetts',
			'first-appearance': 'Ms. Marvel #1',
			publisher: 'Binary',
			alignment: 'good',
		},
		appearance: {
			gender: 'Female',
			race: 'Human-Kree',
			height: ["5'11", '180 cm'],
			weight: ['165 lb', '74 kg'],
			'eye-color': 'Blue',
			'hair-color': 'Blond',
		},
		work: {
			occupation:
				'Former National Aeronautics and Space Administration security Chief, former magazine editor, former freelance writer, former military intelligence officer.',
			base: 'Avengers Mansion',
		},
		connections: {
			'group-affiliation':
				'currently Avengers, former companion to the X-Men (as Carol Danvers), former member of the Starjammers (as Binary), Former Avenger (as Ms. Marvel)',
			relatives:
				'Marie Danvers (mother), Joseph Danvers, Sr. (father), Joseph Danvers, Jr. (brother), Steve Danvers (brother, deceased), Marcus Immortus (Danvers) ("son", deceased)',
		},
		image: {
			url: 'https://www.superherodb.com/pictures2/portraits/10/100/103.jpg',
		},
	},
	{
		id: '38',
		name: 'Aquaman',
		powerstats: {
			intelligence: '81',
			strength: '85',
			speed: '79',
			durability: '80',
			power: '100',
			combat: '80',
		},
		biography: {
			'full-name': 'Orin',
			'alter-egos': 'No alter egos found.',
			aliases: [
				'Dweller in the Depths',
				'Swimmer',
				'Waterbearer',
				'Mental Man',
				'Aquaboy',
				'Water Wraith',
			],
			'place-of-birth': 'Atlantis',
			'first-appearance': 'More Fun Comics #73 (November, 1941)',
			publisher: 'DC Comics',
			alignment: 'good',
		},
		appearance: {
			gender: 'Male',
			race: 'Atlantean',
			height: ["6'1", '185 cm'],
			weight: ['325 lb', '146 kg'],
			'eye-color': 'Blue',
			'hair-color': 'Blond',
		},
		work: {
			occupation: 'Protector of the Seas and Oceans, King of Poseidonis',
			base: 'Atlantean Royal Palace; Poseidonis, Atlantis',
		},
		connections: {
			'group-affiliation':
				'Justice League, Aquaman Family, Atlantean Royal Family; formerly Black Lantern Corps, Justice League International, Justice League Detroit, U.N.',
			relatives:
				'Koryak (son), Arthur Curry, Jr. (son), A.J. (son), Orm Marius (half-brother), Debbie Perkins (half-sister), Drin (adopted brother), Atlanna (mother), Atlan (father), Atlena (aunt), Porm (adopted mother), Tom Curry (adopted father), Mera (wife), Hila (sister-in-law), Haumond (uncle), Kraken (uncle), Honsu (grandfather), Lorelei (grandmother), Manu (ancestor), Nala (ancestor), Fatima (ancestor), Kalunga (ancestor), Gana (ancestor), Fiona (ancestor), Regin (ancestor), Kordax (ancestor), Bazil (ancestor), Cora (ancestor), Illya (ancestor), Dardanus (ancestor), Alloroc (ancestor), Cole (ancestor), Narmea (ancestor), Orin (ancestor), Loma (ancestor), Shalako (ancestor), Thorvall (ancestor)',
		},
		image: {
			url: 'https://www.superherodb.com/pictures2/portraits/10/100/634.jpg',
		},
	},
	{
		id: '332',
		name: 'Hulk',
		powerstats: {
			intelligence: '88',
			strength: '100',
			speed: '63',
			durability: '100',
			power: '98',
			combat: '85',
		},
		biography: {
			'full-name': 'Bruce Banner',
			'alter-egos': 'No alter egos found.',
			aliases: [
				'Annihilator',
				'Captain Universe',
				'Joe Fixit',
				'Mr. Fixit',
				'Mechano',
				'Professor',
				'Jade Jaws',
				'Golly Green Giant',
			],
			'place-of-birth': 'Dayton, Ohio',
			'first-appearance': 'Incredible Hulk #1 (May, 1962)',
			publisher: 'Marvel Comics',
			alignment: 'good',
		},
		appearance: {
			gender: 'Male',
			race: 'Human / Radiation',
			height: ["8'0", '244 cm'],
			weight: ['1400 lb', '630 kg'],
			'eye-color': 'Green',
			'hair-color': 'Green',
		},
		work: {
			occupation: 'Nuclear physicist, Agent of S.H.I.E.L.D.',
			base:
				'(Banner) Hulkbuster Base, New Mexico, (Hulk) mobile, but prefers New Mexico',
		},
		connections: {
			'group-affiliation':
				'Defenders, former leader of the new Hulkbusters, member of the Avengers, Pantheon, Titans Three, the Order, Hulkbusters of Counter-Earth-Franklin, alternate Fantastic Four',
			relatives:
				"Betty Ross Talbot Banner (wife), Brian Banner (father, apparently deceased), Rebecca Banner (mother, deceased), Morris Walters (uncle), Elaine Banner Walters (aunt, deceased), Jennifer Walters (She-Hulk, cousin), Thaddeus E. 'Thunderbolt' Ross (father",
		},
		image: {
			url: 'https://www.superherodb.com/pictures2/portraits/10/100/83.jpg',
		},
	},
	{
		id: '346',
		name: 'Iron Man',
		powerstats: {
			intelligence: '100',
			strength: '85',
			speed: '58',
			durability: '85',
			power: '100',
			combat: '64',
		},
		biography: {
			'full-name': 'Tony Stark',
			'alter-egos': 'No alter egos found.',
			aliases: [
				'Iron Knight',
				'Hogan Potts',
				'Spare Parts Man',
				'Cobalt Man II',
				'Crimson Dynamo',
				'Ironman',
			],
			'place-of-birth': 'Long Island, New York',
			'first-appearance': 'Tales of Suspence #39 (March, 1963)',
			publisher: 'Marvel Comics',
			alignment: 'good',
		},
		appearance: {
			gender: 'Male',
			race: 'Human',
			height: ["6'6", '198 cm'],
			weight: ['425 lb', '191 kg'],
			'eye-color': 'Blue',
			'hair-color': 'Black',
		},
		work: {
			occupation:
				'Inventor, Industrialist; former United States Secretary of Defense',
			base: 'Seattle, Washington',
		},
		connections: {
			'group-affiliation':
				'Avengers, Illuminati, Stark Resilient; formerly S.H.I.E.L.D., leader of Stark Enterprises, the Pro-Registration Superhero Unit, New Avengers, Mighty Avengers, Hellfire Club, Force Works, Avengers West Coast, United States Department of Defense.',
			relatives:
				'Howard Anthony Stark (father, deceased), Maria Stark (mother, deceased), Morgan Stark (cousin), Isaac Stark (ancestor)',
		},
		image: {
			url: 'https://www.superherodb.com/pictures2/portraits/10/100/85.jpg',
		},
	},
	{
		id: '659',
		name: 'Thor',
		powerstats: {
			intelligence: '69',
			strength: '100',
			speed: '83',
			durability: '100',
			power: '100',
			combat: '100',
		},
		biography: {
			'full-name': 'Thor Odinson',
			'alter-egos': 'Rune King Thor',
			aliases: [
				'Donald Blake',
				'Sigurd Jarlson',
				'Jake Olsen',
				'Donar the Mighty',
			],
			'place-of-birth': 'Asgard',
			'first-appearance': 'Journey into Mystery #83 (August, 1962)',
			publisher: 'Rune King Thor',
			alignment: 'good',
		},
		appearance: {
			gender: 'Male',
			race: 'Asgardian',
			height: ["6'6", '198 cm'],
			weight: ['640 lb', '288 kg'],
			'eye-color': 'Blue',
			'hair-color': 'Blond',
		},
		work: {
			occupation: 'King of Asgard; formerly EMS Technician; Physician',
			base: 'New York, New York',
		},
		connections: {
			'group-affiliation': 'Avengers',
			relatives:
				'Odin (father), Gaea (mother), Frigga (step-mother), Loki (step-brother), Vidar (half-brother), Buri (paternal great-grandfather), Bolthorn (maternal great grandfather), Bor (grandfather), Bestla (grandmother), Vili (uncle), Ve (uncle), Sigyn (former sister-in-law), Hela (alleged niece), Jormungand (alleged nephew), Fernis Wolf (alleged nephew)',
		},
		image: {
			url: 'https://www.superherodb.com/pictures2/portraits/10/100/140.jpg',
		},
	},
];

export function getSuperHeroData() {
	return data;
}
