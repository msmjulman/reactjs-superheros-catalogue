import React, { Component } from 'react';
import './App.css';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import Header from './components/Header';
import Sidebar from './components/Sidebar';
import ComicDetails from './components/ComicDetails';
import About from './components/About';
import Home from './components/Home';
import Search from './components/Search';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			toggleNav: false,
		};
	}

	toggleNavigation = () => {
		this.setState({ toggleNav: !this.state.toggleNav });
	};

	render() {
		return (
			<div className="App">
				<Router>
					<Header
						toggleNavigation={this.toggleNavigation}
						toggleState={this.state.toggleNav}
					/>
					<Sidebar
						toggleNavigation={this.toggleNavigation}
						toggleState={this.state.toggleNav}
					/>

					<Switch>
						<Route exact path="/" component={Home} />
						<Route exact path="/search" component={Search} />
						<Route exact path="/comics-details/:id" component={ComicDetails} />
						<Route exact path="/about" component={About} />
					</Switch>
				</Router>
			</div>
		);
	}
}

export default App;
