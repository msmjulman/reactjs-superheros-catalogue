import React, { Component } from 'react';
import axios from 'axios';
import { PuffLoader } from 'react-spinners';
import './Home.css';
import proxy from './cors_anywhere';

class ComicDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {
			dataSuperheros: [],
			isLoading: false,
			error: '',
		};
	}

	componentDidMount() {
		this.setState({ isLoading: true });
		this.loadData();
	}

	loadData = () => {
		const id = this.props.match.params.id;
		// console.log('ID', id);

		axios
			.get(`${proxy}/https://www.superheroapi.com/api/3382079748517645/${id}`, {
				headers: {
					'Content-Type': 'application/json',
					'Access-Control-Allow-Origin': '*',
				},
			})
			.then((response) => {
				// If request is good...
				// console.log(response.data);
				if (this.state.dataSuperheros !== null) {
					const newData = [...this.state.dataSuperheros, response.data];
					this.setState({ dataSuperheros: newData });
				}
				this.setState({ isLoading: false });
			})
			.catch((error) => {
				console.log('error ' + error);
				this.setState({
					error: 'Network Error or Data Fetching Error',
					isLoading: false,
				});
			});
	};
	render() {
		const { dataSuperheros, isLoading, error } = this.state;
		const id = this.props.match.params.id;
		// console.log(dataSuperheros);
		return (
			<div id="content">
				<div id="content-header">
					<p>/ Comic details</p>
				</div>
				<div id="content-body">
					<div className="row d-flex flex-column align-items-start">
						{isLoading ? (
							<div className="loader text-center m-0 w-100">
								{/* <h3 className="lead m-0">...Loading</h3> */}
								<PuffLoader size={100} isLoading />
							</div>
						) : (
							dataSuperheros.map(
								(item) =>
									item['id'] === id && (
										<div
											className="card w-100 mb-3 px-3 border-0"
											key={item['id']}>
											<div className="row no-gutters">
												<div className="col-md-4">
													<p
														className="w-100"
														style={{
															background: 'grey',
															border: ' 2px solid #ff336a',
														}}>
														<img
															src={item['image']['url']}
															className="card-img"
															alt="comic-cover"
														/>
													</p>
												</div>
												<div className="col-md-8">
													<div className="card-body py-0">
														<h2 className="card-title text-center">
															{item['name']}
														</h2>
														<p className="m-2">
															<span className="mr-2 font-weight-bold">
																Full-name :
															</span>
															{item.biography['full-name']}
														</p>
														<p className="m-2">
															<span className="mr-2 font-weight-bold">
																Aliases :
															</span>
															{item.biography['aliases']}
														</p>
														<p className="m-2">
															<span className="mr-2 font-weight-bold">
																Place of birth :
															</span>
															{item.biography['place-of-birth']}
														</p>
														<p className="m-2">
															<span className="mr-2 font-weight-bold">
																Gender :
															</span>
															{item.appearance['gender']}
														</p>
														<p className="m-2">
															<span className="mr-2 font-weight-bold">
																Race :
															</span>
															{item.appearance['race']}
														</p>
														<p className="m-2">
															<span className="mr-2 font-weight-bold">
																Occupation :
															</span>
															{item.work['occupation']}
														</p>
														<p className="m-2">
															<span className="mr-2 font-weight-bold">
																Base :
															</span>
															{item.work['base']}
														</p>
														<p className="m-2">
															<span className="mr-2 font-weight-bold">
																Group affliation :
															</span>
															{item.connections['group-affiliation']}
														</p>
														<p className="m-2">
															<span className="mr-2 font-weight-bold">
																First Appearance :
															</span>
															{item.biography['first-appearance']}
														</p>
														<p className="m-2">
															<span className="mr-2 font-weight-bold">
																Edited by :
															</span>
															{item.biography['publisher']}
														</p>
													</div>
												</div>
											</div>
										</div>
									)
							)
						)}
						{error && (
							<div className="text-center w-100">
								<h3 className="lead text-capitalize">{error}</h3>
							</div>
						)}
						<button
							className="btn btn-primary ml-3 px-md-3 px-sm-2 py-md-2 py-sm-1"
							onClick={() => this.props.history.goBack()}>
							<i className="fa fa-chevron-left mr-2"></i>Back
						</button>
					</div>
				</div>
				<div id="content-footer">© Copyright 2020 - Powered by MSM</div>
			</div>
		);
	}
}

export default ComicDetails;
