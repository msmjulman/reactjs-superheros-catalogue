import React, { Component } from 'react';
import logo from '../images/logo.png';
import avatar from '../images/avatar.jpg';
import { Link } from 'react-router-dom';

export default class Header extends Component {
	render() {
		const { toggleNavigation, toggleState } = this.props;
		// console.log('Header');
		return (
			<header id="main-nav">
				<div id="brand-container">
					<Link to="/" id="brand">
						<img src={logo} alt="logo" title="logo" />
					</Link>
				</div>
				<div id="menu-icon-container">
					<div id="menu-icon" className="">
						{!toggleState ? (
							<button
								id="open"
								className="btn d-lg-none"
								onClick={() => toggleNavigation()}>
								<i className="fa fa-bars" />
							</button>
						) : (
							<button
								id="close"
								className="btn d-lg-none"
								onClick={() => toggleNavigation()}>
								<i className="fas fa-times" />
							</button>
						)}
						<div id="avatar">
							<img src={avatar} alt="my-avatar" />
						</div>
					</div>
				</div>
			</header>
		);
	}
}
