import React, { Component } from 'react';
import '../components/Home.css';
import { Link } from 'react-router-dom';
import ContentWrapper from './ContentWrapper';
import { PuffLoader } from 'react-spinners';
import axios from 'axios';
import proxy from './cors_anywhere';

export default class Content extends Component {
	constructor(props) {
		super(props);
		this.state = {
			dataSuperheros: [],
			isLoading: false,
			error: '',
		};
	}

	componentDidMount() {
		this.setState({ isLoading: true });
		this.loadData();
	}

	loadData = () => {
		const dataAfterLoop = [];
		const homeArr = [106, 644, 620, 157, 38, 332, 346, 659];

		homeArr.forEach((item) => {
			axios
				.get(
					` ${proxy}/https://www.superheroapi.com/api/3382079748517645/${item}`,
					{
						headers: {
							'Content-Type': 'application/json',
							'Access-Control-Allow-Origin': '*',
						},
					}
				)
				.then((response) => {
					// If request is good...
					// console.log(response.data);
					dataAfterLoop.push(response.data);
					this.setState({ dataSuperheros: dataAfterLoop });
					this.setState({ isLoading: false });
				})
				.catch((error) => {
					console.log('error ' + error);
					this.setState({
						error: 'Network Error or Data Fetching Error',
						isLoading: false,
					});
				});
		});
	};

	render() {
		const { dataSuperheros, isLoading, error } = this.state;
		// console.log('RENDER', dataSuperheros);
		return (
			<ContentWrapper navigationTitle="Home">
				<div className="row">
					{isLoading ? (
						<div className="loader text-center m-0 w-100">
							{/* <h3 className="lead m-0">...Loading</h3> */}
							<PuffLoader size={100} isLoading />
						</div>
					) : (
						dataSuperheros.map((item) => (
							<div key={item.id} className="col-lg-3 col-md-4">
								<div className="comic-container d-flex flex-column justify-content-between">
									<p className="image-box">
										<Link to={`/comics-details/${item.id}`}>
											<img
												src={item['image']['url']}
												className="img-fluid w-100"
												alt="comic-cover"
											/>
										</Link>
									</p>
									<div>
										<h5 className="comic-name text-center mb-2">{item.name}</h5>
									</div>
									<Link
										className="btn btn-primary"
										to={`/comics-details/${item.id}`}>
										View more...
									</Link>
								</div>
							</div>
						))
					)}
					{error && (
						<div className="text-center w-100">
							<h3 className="lead text-capitalize">{error}</h3>
						</div>
					)}
				</div>
			</ContentWrapper>
		);
	}
}
