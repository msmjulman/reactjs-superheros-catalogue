import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Sidebar extends Component {
	render() {
		const { toggleState, toggleNavigation } = this.props;
		const expand = toggleState ? 'expand-menu' : '';
		return (
			<aside id="sidebar" className={expand}>
				<ul id="side-menu" onClick={() => toggleNavigation()}>
					<li>
						<Link to="/">
							<i className="fa fa-home" />
							<span className="link-nav">Home</span>
						</Link>
					</li>
					<li>
						<Link to="/search">
							<i className="fa fa-search" />
							<span className="link-nav">Search</span>
						</Link>
					</li>
					<li>
						<Link to="/about">
							<i className="fa fa-info-circle" />
							<span className="link-nav">About</span>
						</Link>
					</li>
				</ul>
			</aside>
		);
	}
}
