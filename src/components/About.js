import React from 'react';
import ContentWrapper from './ContentWrapper';

function About() {
	return (
		<ContentWrapper navigationTitle="About">
			<div className="row">
				<div className="col about">
					<h1 className="text-center">About</h1>
					<p className="text-center mt-5">
						This is an app that lists super heroes.
					</p>
					<p className="text-center">
						In this project I used ReactJs and the data is provided by the
						Superheroapi API.
					</p>
					<p className="text-center mt-5 lead">Just to learn more ...</p>
				</div>
			</div>
		</ContentWrapper>
	);
}

export default About;
