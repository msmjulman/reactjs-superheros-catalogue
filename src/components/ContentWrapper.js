import React from 'react';

const ContentWrapper = (props) => {
	return (
		<div id="content">
			<div id="content-header">
				<p>/ {props.navigationTitle}</p>
			</div>
			<div id="content-body">{props.children}</div>
			<div id="content-footer">© Copyright 2020 - Powered by MSM</div>
		</div>
	);
};

export default ContentWrapper;
