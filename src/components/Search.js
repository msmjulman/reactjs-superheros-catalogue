import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ContentWrapper from './ContentWrapper';
import '../components/Home.css';
import '../components/Search.css';

import axios from 'axios';
import { PuffLoader } from 'react-spinners';
import proxy from './cors_anywhere';

export default class Search extends Component {
	constructor(props) {
		super(props);
		this.state = {
			dataSuperheros: [],
			query: '',
			errorRequestMessage: null,
			error: '',
			isLoading: false,
		};
	}

	loadData = async (e) => {
		e.preventDefault();

		this.setState({ isLoading: true });

		await axios
			.get(
				`${proxy}/https://www.superheroapi.com/api/3382079748517645/search/${this.state.query}`,
				{
					headers: {
						'Content-Type': 'application/json',
						'Access-Control-Allow-Origin': '*',
					},
				}
			)
			.then((response) => {
				// If request is good...
				// console.log(response.data.results);
				this.setState({
					dataSuperheros: response.data.results,
					errorRequestMessage: response.data.error,
				});
				this.setState({ isLoading: false });
			})
			.catch((error) => {
				console.log('error ' + error);
				this.setState({
					error: 'Network Error or Data Fetching Error',
					isLoading: false,
				});
			});
	};

	handleChange = (e) => {
		this.setState({ query: e.target.value });
	};

	render() {
		const {
			dataSuperheros,
			errorRequestMessage,
			error,
			isLoading,
		} = this.state;
		// console.log('Error Request', errorRequestMessage);

		return (
			<ContentWrapper navigationTitle="Search">
				<div className="row d-flex flex-row justify-content-center">
					<form
						className=" form-inline align-items-start mt-3 mb-3 px-3"
						onSubmit={this.loadData}>
						<div className="form-group mx-sm-3 mb-0 ">
							<label htmlFor="inputSearch" className="sr-only">
								Search
							</label>
							<input
								className="form-control"
								id="inputSearch"
								type="text"
								placeholder="Find your superhero"
								name="search"
								value={this.state.query}
								onChange={this.handleChange}
							/>
						</div>
						<button type="submit" className="btn btn-primary py-2">
							Search
						</button>
					</form>
				</div>
				<div className="row mt-4">
					{isLoading ? (
						<div className="loader text-center  w-100">
							{/* <h3 className="lead ">...Loading</h3> */}
							<PuffLoader size={100} isLoading />
						</div>
					) : (
						dataSuperheros &&
						dataSuperheros.map((item) => (
							<div key={item.id} className="col-lg-3 col-md-4">
								<div className="comic-container d-flex flex-column justify-content-between">
									<p className="image-box">
										<Link to={`/comics-details/${item['id']}`}>
											<img
												src={item['image']['url']}
												className="img-fluid w-100"
												alt="comic-cover"
											/>
										</Link>
									</p>
									<div>
										<h5 className="comic-name text-center mb-2">
											{item['name']}
										</h5>
									</div>
									<Link
										className="btn btn-primary"
										to={`/comics-details/${item['id']}`}>
										View more...
									</Link>
								</div>
							</div>
						))
					)}
					{errorRequestMessage && (
						<div className="error text-center w-100">
							<h3 className="lead text-capitalize">{errorRequestMessage}</h3>
						</div>
					)}
					{error && (
						<div className="error text-center w-100">
							<h3 className="lead text-capitalize">{error}</h3>
						</div>
					)}
				</div>
			</ContentWrapper>
		);
	}
}
